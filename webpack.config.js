const path = require("path");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const {HotModuleReplacementPlugin} = require('webpack');

const isProd = process.env.NODE_ENV === "production";
const isDev = !isProd;
const filename = (ext) => (isDev ? `bundle.${ext}` : `bundle.[hash].${ext}`);

module.exports = {
    target: isDev ? 'web' : 'browserslist',
  context: path.resolve(__dirname, "src"),
  mode: 'development',
  entry: ["@babel/polyfill", "/index.js"],
  output: {
    filename: filename("js"),
    path: path.resolve(__dirname, "dist"),
  },
  devtool: isDev ? "source-map" : false,
  devServer: {
    hot: isDev,
    port: 8080,
    open: true,
  },
  plugins: [
    new HTMLWebpackPlugin({
      template: "index.html",
    }),
    new MiniCssExtractPlugin({
      filename: filename("css"),
    }),
    new CleanWebpackPlugin(),
    new HotModuleReplacementPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
            plugins: ["@babel/plugin-proposal-class-properties"],
          },
        },
      },
    ],
  },
};
